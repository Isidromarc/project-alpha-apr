from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate, get_user_model
from accounts.forms import LoginForm, RegisterForm


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            # custom form clean() method guarantees an existing user
            login(request, user)
            return redirect("home")
    else:
        form = LoginForm()
    context = {"login_form": form}
    return render(request, "accounts/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def register_view(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = get_user_model().objects.create_user(
                username=username, password=password
            )
            login(request, user)
            return redirect("list_projects")
    else:
        form = RegisterForm()
    context = {"register_form": form}
    return render(request, "registration/signup.html", context)
