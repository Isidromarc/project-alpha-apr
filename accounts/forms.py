from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model


class LoginForm(forms.Form):
    username = forms.CharField(
        label="Username:", max_length=150, required=True
    )
    password = forms.CharField(
        label="Password:",
        max_length=150,
        widget=forms.PasswordInput,
        required=True,
    )

    # overide parent clean() method.
    # will allow invalid user error to be shown when
    # logging in with the wrong username/password
    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        clean_username = cleaned_data["username"]
        clean_password = cleaned_data["password"]

        if not authenticate(username=clean_username, password=clean_password):
            raise forms.ValidationError(
                "Invalid username and password", code="invalid user"
            )

        return cleaned_data


class RegisterForm(forms.Form):
    username = forms.CharField(
        label="Username:", max_length=150, required=True
    )
    password = forms.CharField(
        label="Password:",
        max_length=150,
        widget=forms.PasswordInput,
        required=True,
    )
    password_confirmation = forms.CharField(
        label="Confirm Password:",
        max_length=150,
        widget=forms.PasswordInput,
        required=True,
    )

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        clean_username = cleaned_data["username"]
        clean_password = cleaned_data["password"]
        clean_password2 = cleaned_data["password_confirmation"]

        errors = []

        if get_user_model().objects.filter(username=clean_username):
            errors.append(
                forms.ValidationError(
                    "Username already exists", code="invalid username"
                )
            )

        if clean_password != clean_password2:
            errors.append(
                forms.ValidationError(
                    "the passwords do not match", code="password mismatch"
                )
            )

        if errors:
            raise forms.ValidationError(errors)

        return cleaned_data
